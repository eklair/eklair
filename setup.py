#!/usr/bin/env python

from setuptools import setup

__version__ = '0.1.5'

setup(
    name='eklair',
    version=__version__,
    description='Instant git',
    author='Noel Martignoni',
    author_email='noel@martignoni.fr',
    url='https://gitlab.com/eklair/eklair',
    scripts = ['scripts/eklair'],
    install_requires=['future', 'PyGithub'],
    packages = [],
)
